# 查看数据库：安装studio 3T 或者 idea 安装插件MongoDB
# 因為在沒有指定資料庫路徑時會預設使用 /data/db 這個路徑，但在 Mac 中找不到這個資料夾。解決方法就是建立這個資料夾，執行：
sudo mkdir -p /data/db

在終端機執行：

brew services start mongo

察看目前已安裝的 services 和各個 services 的啟動狀態：

brew services list

若要關閉 MongoDB services：

brew services stop mongo

可以視情況使用 mongod 或是 brew services 來啟動 MongoDB。